package edu.westga.cs.schoolgrades.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

class TestSumOfGradesStrategyDropLowestGetComposite {

	@Test
	void testGetValueShouldReturnAverageOfTwoTestScoresEqualTo50() {
		SimpleGrade testSimpleGrade1 = new SimpleGrade(0);
		SimpleGrade testSimpleGrade2 = new SimpleGrade(100);
		Strategy testStrategy = new SumOfGradesStrategy();
		Strategy testDrop = new DropLowestStrategy(testStrategy);
		CompositeGrade testCompositeGrade = new CompositeGrade(testDrop);
		testCompositeGrade.addGrade(testSimpleGrade1);
		testCompositeGrade.addGrade(testSimpleGrade2);
		assertEquals(testCompositeGrade.getValue(), 100.0);
	}
	
	@Test
	void testGetValueShouldReturnAverageOfFourTestScoresEqualTo75() {
		SimpleGrade testSimpleGrade1 = new SimpleGrade(50);
		SimpleGrade testSimpleGrade2 = new SimpleGrade(100);
		SimpleGrade testSimpleGrade3 = new SimpleGrade(75);
		SimpleGrade testSimpleGrade4 = new SimpleGrade(75);
		Strategy testStrategy = new SumOfGradesStrategy();
		Strategy testDrop = new DropLowestStrategy(testStrategy);
		CompositeGrade testCompositeGrade = new CompositeGrade(testDrop);
		testCompositeGrade.addGrade(testSimpleGrade1);
		testCompositeGrade.addGrade(testSimpleGrade2);
		testCompositeGrade.addGrade(testSimpleGrade3);
		testCompositeGrade.addGrade(testSimpleGrade4);
		assertEquals(testCompositeGrade.getValue(), 250);
	}
	
	@Test
	void testGetValueShouldReturnSingleValueWhenThereIsOnlyOneGrade() {
		SimpleGrade testSimpleGrade = new SimpleGrade(100);
		Strategy testStrategy = new SumOfGradesStrategy();
		Strategy testDrop = new DropLowestStrategy(testStrategy);
		CompositeGrade testCompositeGrade = new CompositeGrade(testDrop);
		testCompositeGrade.addGrade(testSimpleGrade);
		assertEquals(testCompositeGrade.getValue(), 100.0);
	}
	
	@Test
	void testDropLowestShouldNotContainGradeWithAValueOf0AfterDropping0() {
		SimpleGrade testSimpleGrade1 = new SimpleGrade(50);
		SimpleGrade testSimpleGrade2 = new SimpleGrade(0);
		SimpleGrade testSimpleGrade3 = new SimpleGrade(100);
		SimpleGrade testSimpleGrade4 = new SimpleGrade(150);
		Strategy testStrategy = new SumOfGradesStrategy();
		DropLowestStrategy testDrop = new DropLowestStrategy(testStrategy);
		CompositeGrade testCompositeGrade = new CompositeGrade(testStrategy);
		testCompositeGrade.addGrade(testSimpleGrade1);
		testCompositeGrade.addGrade(testSimpleGrade2);
		testCompositeGrade.addGrade(testSimpleGrade3);
		testCompositeGrade.addGrade(testSimpleGrade4);
		assertEquals(false, testDrop.dropLowest(testCompositeGrade.getListOfGrades()).contains(testSimpleGrade2));
			
	}
	
	@Test
	public void testAddNullInSumStrategyGradeDropLowestThrowsException() {
		assertThrows(IllegalArgumentException.class, () -> {
			
			new DropLowestStrategy(null);
		
		});
	}
	
}
