package edu.westga.cs.schoolgrades.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestAverageGradesStrategyDropLowestGetComposite {

	@Test
	void testGetCompositeShouldReturnAverageOfTwoTestScoresEqualTo100AfterDroppingLowest() {
		SimpleGrade testSimpleGrade1 = new SimpleGrade(0);
		SimpleGrade testSimpleGrade2 = new SimpleGrade(100);
		Strategy testStrategy = new AverageOfGradesStrategy();
		Strategy testDrop = new DropLowestStrategy(testStrategy);
		CompositeGrade testCompositeGrade = new CompositeGrade(testDrop);
		testCompositeGrade.addGrade(testSimpleGrade1);
		testCompositeGrade.addGrade(testSimpleGrade2);
		assertEquals(testCompositeGrade.getValue(), 100);
	}
	
	@Test
	void testGetCompositeShouldReturnAverageOfFourTestScoresEqualTo80AfterDroppingLowest() {
		SimpleGrade testSimpleGrade1 = new SimpleGrade(50);
		SimpleGrade testSimpleGrade2 = new SimpleGrade(100);
		SimpleGrade testSimpleGrade3 = new SimpleGrade(80);
		SimpleGrade testSimpleGrade4 = new SimpleGrade(60);
		Strategy testStrategy = new AverageOfGradesStrategy();
		Strategy testDrop = new DropLowestStrategy(testStrategy);
		CompositeGrade testCompositeGrade = new CompositeGrade(testDrop);
		testCompositeGrade.addGrade(testSimpleGrade1);
		testCompositeGrade.addGrade(testSimpleGrade2);
		testCompositeGrade.addGrade(testSimpleGrade3);
		testCompositeGrade.addGrade(testSimpleGrade4);
		assertEquals(testCompositeGrade.getValue(), 80);
	}
	
	@Test
	void testGetValueShouldReturnSingleValueWhenThereIsOnlyOneGrade() {
		SimpleGrade testSimpleGrade = new SimpleGrade(100);
		Strategy testStrategy = new AverageOfGradesStrategy();
		Strategy testDrop = new DropLowestStrategy(testStrategy);
		CompositeGrade testCompositeGrade = new CompositeGrade(testDrop);
		testCompositeGrade.addGrade(testSimpleGrade);
		assertEquals(testCompositeGrade.getValue(), 100.0);
	}

	@Test
	void testDropLowestShouldNotContainGradeWithAValueOf0AfterDropping0() {
		SimpleGrade testSimpleGrade1 = new SimpleGrade(50);	
		SimpleGrade testSimpleGrade3 = new SimpleGrade(100);
		SimpleGrade testSimpleGrade4 = new SimpleGrade(150);
		SimpleGrade testSimpleGrade2 = new SimpleGrade(0);
		Strategy testStrategy = new AverageOfGradesStrategy();
		DropLowestStrategy testDrop = new DropLowestStrategy(testStrategy);
		CompositeGrade testCompositeGrade = new CompositeGrade(testDrop);
		testCompositeGrade.addGrade(testSimpleGrade1);
		testCompositeGrade.addGrade(testSimpleGrade2);
		testCompositeGrade.addGrade(testSimpleGrade3);
		testCompositeGrade.addGrade(testSimpleGrade4);
		assertEquals(false, testDrop.dropLowest(testCompositeGrade.getListOfGrades()).contains(testSimpleGrade2));
			
	}
	
	@Test
	public void testGetCompositeFromNullListThrowsException() {
		assertThrows(IllegalArgumentException.class, () -> {
			Strategy testStrategy = new SumOfGradesStrategy();
			Strategy testDrop = new DropLowestStrategy(testStrategy);
			testDrop.getComposite(null);
		});
	}
}
