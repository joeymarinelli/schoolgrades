package edu.westga.cs.schoolgrades.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestWeightedGradeGetValue {

	@Test
	void testWeightSimpleGradeOf50WithMultiplierOf50PercentShouldReturnValueOf25() {
		Grade testGrade = new SimpleGrade(50);
		Grade testWeightedGrade = new WeightedGrade(testGrade, 0.5);
		assertEquals(testWeightedGrade.getValue(), 25);
	}
	
	@Test
	void testWeightCompositeGradeWithAverageStrategyOf50WithMultiplierOf50PercentShouldReturnValueOf25() {
		Grade testGrade1 = new SimpleGrade(0);
		Grade testGrade2 = new SimpleGrade(100);
		Strategy testStrategy = new AverageOfGradesStrategy();
		CompositeGrade testCompositeGrade = new CompositeGrade(testStrategy);
		testCompositeGrade.addGrade(testGrade1);
		testCompositeGrade.addGrade(testGrade2);
		Grade testWeightedGrade = new WeightedGrade(testCompositeGrade, 0.5);
		assertEquals(testWeightedGrade.getValue(), 25);
	}
	
	@Test
	void testWeightCompositeGradeWithAverageStrategyOf50WithMultiplierOf3ShouldReturnValueOf300AfterLowestIsDropped() {
		Grade testGrade1 = new SimpleGrade(20);
		Grade testGrade2 = new SimpleGrade(50);
		Strategy testSumStrategy = new SumOfGradesStrategy();
		Strategy dropStrategy = new DropLowestStrategy(testSumStrategy);
		CompositeGrade testCompositeGrade = new CompositeGrade(dropStrategy);
		testCompositeGrade.addGrade(testGrade1);
		testCompositeGrade.addGrade(testGrade2);
		Grade testWeightedGrade = new WeightedGrade(testCompositeGrade, 0.5);
		assertEquals(testWeightedGrade.getValue(), 25);
	}

	@Test
	void testWeightCompositeGradeWithSumStrategyOf50WithMultiplierOf50PercentShouldReturnValueOf50() {
		Grade testGrade1 = new SimpleGrade(0);
		Grade testGrade2 = new SimpleGrade(100);
		Strategy testStrategy = new SumOfGradesStrategy();
		CompositeGrade testCompositeGrade = new CompositeGrade(testStrategy);
		testCompositeGrade.addGrade(testGrade1);
		testCompositeGrade.addGrade(testGrade2);
		Grade testWeightedGrade = new WeightedGrade(testCompositeGrade, 0.5);
		assertEquals(testWeightedGrade.getValue(), 50);
	}
	
	@Test
	void testWeightCompositeGradeWithSumStrategyOf100WithMultiplierOf50PercentShouldReturnValueOf50AfterLowestIsDropped() {
		Grade testGrade1 = new SimpleGrade(20);
		Grade testGrade2 = new SimpleGrade(50);
		Grade testGrade3 = new SimpleGrade(50);
		Strategy testSumStrategy = new SumOfGradesStrategy();
		Strategy dropStrategy = new DropLowestStrategy(testSumStrategy);
		CompositeGrade testCompositeGrade = new CompositeGrade(dropStrategy);
		testCompositeGrade.addGrade(testGrade1);
		testCompositeGrade.addGrade(testGrade2);
		testCompositeGrade.addGrade(testGrade3);
		Grade testWeightedGrade = new WeightedGrade(testCompositeGrade, 0.5);
		assertEquals(testWeightedGrade.getValue(), 50);
	}
	
	@Test
	void testWeightGradeShouldThrowExceptionWhenGradeIsNull() {
		assertThrows(IllegalArgumentException.class, () -> {
			new WeightedGrade(null, 0.5);
		});
	}
	
	@Test
	void testWeightGradeShouldThrowExceptionWhenWeightIsNegative() {
		assertThrows(IllegalArgumentException.class, () -> {
			Grade testGrade = new SimpleGrade(50);
			new WeightedGrade(testGrade, -2);
		});
	}
}
