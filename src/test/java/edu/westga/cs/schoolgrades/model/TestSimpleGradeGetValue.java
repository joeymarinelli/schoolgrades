package edu.westga.cs.schoolgrades.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestSimpleGradeGetValue {

	/** 
	 * This test verifies that the getValue method of a SimpleGrade object with a value of 0.0 returns 0.0
	 */
	@Test
	public void testGradeValueOfZeroShouldReturnZero() {
		SimpleGrade testGrade = new SimpleGrade(0);
		assertEquals(testGrade.getValue(), 0.0);
	}
	
	/** 
	 * This test verifies that the getValue method of a SimpleGrade object with a value of 100 returns 100
	 */
	@Test
	public void testGradeValueOf100ShouldReturn100() {
		SimpleGrade testGrade = new SimpleGrade(100);
		assertEquals(testGrade.getValue(), 100.0);
	}
	
	/**
	 * This test will verify that the setGrade function works properly
	 */
	@Test
	public void testSetGradeOfSimpleGradeTo85() {
		SimpleGrade testGrade = new SimpleGrade(80);
		testGrade.setGrade(85);
		assertEquals(85, testGrade.getValue());
	}
	
	/**
	 * This test will verify that a SimpleGrade with a negative value will throw an IllegalArgumentException
	 */
	@Test
	public void testNegativeGradeValueThrowsException() {
		assertThrows(IllegalArgumentException.class, () -> {
		 new SimpleGrade(-10);
		});
	}
	
	/**
	 * This test will verify that setGrade with a null value will throw an IllegalArgumentException
	 */
	@Test
	public void testSetGradeToNegativeException() {
		assertThrows(IllegalArgumentException.class, () -> {
		 SimpleGrade testGrade = new SimpleGrade(10);
		 testGrade.setGrade(-10);
		});
	}

}
