package edu.westga.cs.schoolgrades.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestSumOfGradesStrategyGetComposite {
	
	@Test
	void testGetValueShouldReturnAverageOfOneTestScoreEqualTo50() {
		SimpleGrade testSimpleGrade1 = new SimpleGrade(50);
		Strategy testStrategy = new SumOfGradesStrategy();
		CompositeGrade testCompositeGrade = new CompositeGrade(testStrategy);
		testCompositeGrade.addGrade(testSimpleGrade1);
		assertEquals(testCompositeGrade.getValue(), 50.0);
	}
	

	@Test
	void testGetValueShouldReturnAverageOfTwoTestScoresEqualTo50() {
		SimpleGrade testSimpleGrade1 = new SimpleGrade(0);
		SimpleGrade testSimpleGrade2 = new SimpleGrade(100);
		Strategy testStrategy = new SumOfGradesStrategy();
		CompositeGrade testCompositeGrade = new CompositeGrade(testStrategy);
		testCompositeGrade.addGrade(testSimpleGrade1);
		testCompositeGrade.addGrade(testSimpleGrade2);
		assertEquals(testCompositeGrade.getValue(), 100.0);
	}
	
	@Test
	void testGetValueShouldReturnAverageOfFourTestScoresEqualTo75() {
		SimpleGrade testSimpleGrade1 = new SimpleGrade(50);
		SimpleGrade testSimpleGrade2 = new SimpleGrade(100);
		SimpleGrade testSimpleGrade3 = new SimpleGrade(75);
		SimpleGrade testSimpleGrade4 = new SimpleGrade(75);
		Strategy testStrategy = new SumOfGradesStrategy();
		CompositeGrade testCompositeGrade = new CompositeGrade(testStrategy);
		testCompositeGrade.addGrade(testSimpleGrade1);
		testCompositeGrade.addGrade(testSimpleGrade2);
		testCompositeGrade.addGrade(testSimpleGrade3);
		testCompositeGrade.addGrade(testSimpleGrade4);
		assertEquals(testCompositeGrade.getValue(), 300);
	}
}
