package edu.westga.cs.schoolgrades.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestCompositeGradeGetValue {

	@Test
	void testGetValueShouldReturnAverageOfTwoTestScoresEqualTo50() {
		SimpleGrade testSimpleGrade1 = new SimpleGrade(0);
		SimpleGrade testSimpleGrade2 = new SimpleGrade(100);
		Strategy testStrategy = new AverageOfGradesStrategy();
		CompositeGrade testCompositeGrade = new CompositeGrade(testStrategy);
		testCompositeGrade.addGrade(testSimpleGrade1);
		testCompositeGrade.addGrade(testSimpleGrade2);
		assertEquals(testCompositeGrade.getValue(), 50.0);
	}
	
	@Test
	void testGetValueShouldReturnASumOfFourScoresEqualTo300() {
		SimpleGrade testSimpleGrade1 = new SimpleGrade(50);
		SimpleGrade testSimpleGrade2 = new SimpleGrade(100);
		SimpleGrade testSimpleGrade3 = new SimpleGrade(75);
		SimpleGrade testSimpleGrade4 = new SimpleGrade(75);
		Strategy testStrategy2 = new AverageOfGradesStrategy();
		Strategy testStrategy = new SumOfGradesStrategy();
		CompositeGrade testCompositeGrade = new CompositeGrade(testStrategy2);
		testCompositeGrade.setStrategy(testStrategy);
		testCompositeGrade.addGrade(testSimpleGrade1);
		testCompositeGrade.addGrade(testSimpleGrade2);
		testCompositeGrade.addGrade(testSimpleGrade3);
		testCompositeGrade.addGrade(testSimpleGrade4);
		assertEquals(testCompositeGrade.getValue(), 300);
	}

}
