package edu.westga.cs.schoolgrades.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestCompositeGradeAddGrade {

	@Test
	void testCompositeGradeAddGradeOf90ShouldHave1GradeWithAValueOf90() {
		SimpleGrade testSimpleGrade = new SimpleGrade(90);
		Strategy testStrategy = new AverageOfGradesStrategy();
		CompositeGrade testCompositeGrade = new CompositeGrade(testStrategy);
		testCompositeGrade.addGrade(testSimpleGrade);
		assertEquals(testCompositeGrade.getListOfGrades().get(0).getValue(), 90);
	}
	
	@Test
	void testCompositeGradeAddGradeOfZeroShouldHave1GradeWithAValueOfZero() {
		SimpleGrade testSimpleGrade = new SimpleGrade(0);
		Strategy testStrategy = new AverageOfGradesStrategy();
		CompositeGrade testCompositeGrade = new CompositeGrade(testStrategy);
		testCompositeGrade.addGrade(testSimpleGrade);
		assertEquals(testCompositeGrade.getListOfGrades().get(0).getValue(), 0);
	}
	

	
	@Test
	public void testNullStrategyInCompositeGradeThrowsException() {
		assertThrows(IllegalArgumentException.class, () -> {
			new CompositeGrade(null);
		});
	}
	
	@Test
	public void testSetNullStrategyInCompositeGradeThrowsException() {
		assertThrows(IllegalArgumentException.class, () -> {
			Strategy testStrategy1 = new SumOfGradesStrategy();
			CompositeGrade testComposite = new CompositeGrade(testStrategy1);
			testComposite.setStrategy(null);
		});
	}

}
