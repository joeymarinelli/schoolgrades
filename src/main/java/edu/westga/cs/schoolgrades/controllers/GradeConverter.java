package edu.westga.cs.schoolgrades.controllers;

import edu.westga.cs.schoolgrades.model.Grade;
import edu.westga.cs.schoolgrades.model.SimpleGrade;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.util.StringConverter;

public class GradeConverter extends StringConverter<Grade> {
	private TextFieldListCell<Grade> cell;

	public GradeConverter(TextFieldListCell<Grade> cell) {
		this.cell = cell;
	}

	@Override
	public String toString(Grade object) {
		return object.getValue() + "";
	}

	@Override
	public Grade fromString(String string) {
		SimpleGrade grade = (SimpleGrade) this.cell.getItem();
		try {
			double value = Double.parseDouble(string);
			grade.setGrade(value);
		} catch(NumberFormatException nfe) {
			Alert emptyAlert = new Alert(AlertType.WARNING, "Enter a grade");
			emptyAlert.show();
		}
		return grade;
	}

}
