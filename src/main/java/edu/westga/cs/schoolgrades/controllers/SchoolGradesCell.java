package edu.westga.cs.schoolgrades.controllers;

import edu.westga.cs.schoolgrades.model.Grade;
import javafx.scene.control.cell.TextFieldListCell;

public class SchoolGradesCell extends TextFieldListCell<Grade> {

	@Override
	public void updateItem(Grade item, boolean empty) {
		super.updateItem(item, empty);
		if (item != null) {
			this.setEditable(true);
			GradeConverter converter = new GradeConverter(this);
			this.setConverter(converter);
			setText(item.getValue() + "");

		}
	}

}
