package edu.westga.cs.schoolgrades.controllers;


import edu.westga.cs.schoolgrades.model.AverageOfGradesStrategy;
import edu.westga.cs.schoolgrades.model.CompositeGrade;
import edu.westga.cs.schoolgrades.model.DropLowestStrategy;
import edu.westga.cs.schoolgrades.model.Grade;
import edu.westga.cs.schoolgrades.model.SimpleGrade;
import edu.westga.cs.schoolgrades.model.SumOfGradesStrategy;
import edu.westga.cs.schoolgrades.model.WeightedGrade;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.util.Callback;

/**
 * This is the SchoolGradesController class. It will hold the methods that allow the user to interact with the GUI
 * @author Joey Marinelli
 * @version 10/30/18
 *
 */
public class SchoolGradesController {

	private CompositeGrade compositeQuizGrade;
	private CompositeGrade compositeHomeworkGrade;
	private CompositeGrade compositeExamGrade;

	
	@FXML
	private ListView<Grade> quizGrades;
	
	@FXML
	private ListView<Grade> homeworkGrades;
	
	@FXML 
	private ListView<Grade> examGrades;
	
	@FXML
	private TextField quizSubtotal;
	
	@FXML
	private TextField homeworkSubtotal;
	
	@FXML
	private TextField examSubtotal;
	
	@FXML
	private TextField finalGradeTotal;

	
	public SchoolGradesController() {

		this.compositeExamGrade = new CompositeGrade(new AverageOfGradesStrategy());
		this.compositeHomeworkGrade = new CompositeGrade(new DropLowestStrategy(new AverageOfGradesStrategy()));
		this.compositeQuizGrade = new CompositeGrade(new SumOfGradesStrategy());
		
		this.addGrades();
		
	}
	
	@FXML
	public void initialize() {

		this.quizGrades.setItems(this.compositeQuizGrade.getListOfGrades());
		this.quizSubtotalProperty();
		
		this.homeworkGrades.setItems(this.compositeHomeworkGrade.getListOfGrades());
		this.homeworkSubtotalProperty();
		
		this.examGrades.setItems(this.compositeExamGrade.getListOfGrades());		
		this.examSubtotalProperty();
	
		this.finalGradeProperty();

			this.quizGradeFactory();
			this.homeworkGradeFactory();
			this.examGradeFactory();

	}
	
	
	private void addQuiz() {
		this.compositeQuizGrade.addGrade(new SimpleGrade(0));
	}
	

	private void addHomework() {
		this.compositeHomeworkGrade.addGrade(new SimpleGrade(0));
	}
	
	private void addExam() {
		this.compositeExamGrade.addGrade(new SimpleGrade(0));
	}
	
	/**
	 * Factory method to make grades appear as numeric values instead of objects and makes them editable
	 * @return SchoolGradesCell object
	 */
	private void quizGradeFactory() {

		this.quizGrades.setCellFactory(new Callback<ListView<Grade>, ListCell<Grade>>() {			
						@Override
						public ListCell<Grade> call(ListView<Grade> list) {
							return new SchoolGradesCell();	
						}			
					});

	}
	
	/**
	 * Factory method to make grades appear as numeric values instead of objects  and makes them editable
	 * @return SchoolGradesCell object
	 */
	private void homeworkGradeFactory() {
		this.homeworkGrades.setCellFactory(new Callback<ListView<Grade>, ListCell<Grade>>() {			
			@Override
			public ListCell<Grade> call(ListView<Grade> list) {
				return new SchoolGradesCell();	
			}			
		});
	}
	
	/**
	 * Factory method to make grades appear as numeric values instead of objects  and makes them editable
	 * @return SchoolGradesCell object
	 */
	private void examGradeFactory() {
		this.examGrades.setCellFactory(new Callback<ListView<Grade>, ListCell<Grade>>() {			
			@Override
			public ListCell<Grade> call(ListView<Grade> list) {
				return new SchoolGradesCell();	
			}			
		});
	}
	
	
	
	private void addGrades() {
		SimpleGrade quiz1 = new SimpleGrade(20);
		SimpleGrade quiz2 = new SimpleGrade(15);
		SimpleGrade quiz3 = new SimpleGrade(18);
		SimpleGrade quiz4 = new SimpleGrade(10);
		this.compositeQuizGrade.addGrade(quiz1);
		this.compositeQuizGrade.addGrade(quiz2);
		this.compositeQuizGrade.addGrade(quiz3);
		this.compositeQuizGrade.addGrade(quiz4);
		
		
		SimpleGrade hw1 = new SimpleGrade(98);
		SimpleGrade hw2 = new SimpleGrade(75);
		SimpleGrade hw3 = new SimpleGrade(44);
		SimpleGrade hw4 = new SimpleGrade(86);
		this.compositeHomeworkGrade.addGrade(hw1);
		this.compositeHomeworkGrade.addGrade(hw2);
		this.compositeHomeworkGrade.addGrade(hw3);
		this.compositeHomeworkGrade.addGrade(hw4);
		
		SimpleGrade exam1 = new SimpleGrade(90);
		SimpleGrade exam2 = new SimpleGrade(93);
		this.compositeExamGrade.addGrade(exam1);
		this.compositeExamGrade.addGrade(exam2);
		
	}
	
	@FXML
	private void handleAddQuiz() throws Exception {
		this.addQuiz();
	}
	
	@FXML
	private void handleAddHomework() throws Exception {
		this.addHomework();
	}
	
	@FXML 
	private void handleAddExam() throws Exception {
		this.addExam();
	}
	
	@FXML 
	private void handleRecalculate() throws Exception {
		this.examSubtotalProperty();
		this.homeworkSubtotalProperty();
		this.quizSubtotalProperty();
		this.finalGradeProperty();
		
	}
	
	/**
	 * This is the private helper method that binds the composite quiz grade to the quiz subtotal
	 */
	private void quizSubtotalProperty() {
		DoubleProperty quizSubtotalProperty = new SimpleDoubleProperty();
		quizSubtotalProperty.set(this.compositeQuizGrade.getValue());
		ObservableValue<? extends String> quizSubtotalString = quizSubtotalProperty.asString();
		this.quizSubtotal.textProperty().bind(quizSubtotalString);
	}
	
	/**
	 * This is the private helper method that binds the composite homework grade to the homework subtotal
	 */
	private void homeworkSubtotalProperty() {
		DoubleProperty homeworkSubtotalProperty = new SimpleDoubleProperty();
		homeworkSubtotalProperty.set(this.compositeHomeworkGrade.getValue());
		ObservableValue<? extends String> homeworkSubtotalString = homeworkSubtotalProperty.asString();
		this.homeworkSubtotal.textProperty().bind(homeworkSubtotalString);
	}
	
	/**
	 * This is the private helper method that binds the composite exam grade to the exam subtotal
	 */
	private void examSubtotalProperty() {
		DoubleProperty examSubtotalProperty = new SimpleDoubleProperty();
		examSubtotalProperty.set(this.compositeExamGrade.getValue());
		ObservableValue<? extends String> examSubtotalString = examSubtotalProperty.asString();
		this.examSubtotal.textProperty().bind(examSubtotalString);
	}
	
	/**
	 * This is the private helper method that binds the composite exam grade to the exam subtotal
	 */
	private void finalGradeProperty() {
		DoubleProperty finalGradeProperty = new SimpleDoubleProperty();
		finalGradeProperty.set(new WeightedGrade(this.compositeHomeworkGrade, .3).getValue()
				+ new WeightedGrade(this.compositeQuizGrade, .2).getValue()
				+ new WeightedGrade(this.compositeExamGrade, .5).getValue());
		ObservableValue<? extends String> finalGradeString = finalGradeProperty.asString();
		this.finalGradeTotal.textProperty().bind(finalGradeString);
	}

}
