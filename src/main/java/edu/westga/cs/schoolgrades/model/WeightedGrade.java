package edu.westga.cs.schoolgrades.model;

/**
 * This is the WeightedGrade class. It will apply a multiplier to a grade object. 
 * @author Joey
 *
 */
public class WeightedGrade implements Grade {
	private Grade gradeToBeWeighted;
	private double weight;
	
	/**
	 * This is the constructor of the WeightedGrade class. It will accept a Grade object and a value used as a multiplier
	 * @param gradeToBeWeighted is the grade object getting weighted
	 * @param weight is the multiplier used with the grade object.
	 */
	public WeightedGrade(Grade gradeToBeWeighted, double weight) {
		if (gradeToBeWeighted == null) {
			throw new IllegalArgumentException("Must include a Grade object to be weighted");
		}
		if (weight < 0 || weight > 1) {
			throw new IllegalArgumentException("Weight must be between 0 and 1");
		}
		this.gradeToBeWeighted = gradeToBeWeighted;
		this.weight = weight;
	}

	/**
	 * This method returns the value of the grade after applying the weight. 
	 */
	@Override
	public double getValue() {		
		return this.gradeToBeWeighted.getValue() * this.weight;
	}

}
