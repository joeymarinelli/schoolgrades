package edu.westga.cs.schoolgrades.model;

import javafx.collections.ObservableList;

/**
 * This is the AverageOfGradesStrategy class. It will apply a grading strategy to take the average 
 * value of a list of grades. 
 * @author Joey Marinelli
 * @version 10/24/18
 *
 */
public class AverageOfGradesStrategy extends SumOfGradesStrategy{

	/**
	 * This method will return the average of the list of grades
	 * @return averageGradeValue
	 */
	@Override
	public double getComposite(ObservableList<Grade> listOfGrades) {

		return super.getComposite(listOfGrades) / listOfGrades.size();
	}
	
}
