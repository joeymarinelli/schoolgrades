package edu.westga.cs.schoolgrades.model;

import javafx.collections.ObservableList;
/**
 * THis is the strategy interface. Its methods will return a composite grade value, a list of grades and can add a grade. 
 * @author Joey Marinelli
 * @version 10/24/18
 *
 */
public interface Strategy {
	public double getComposite(ObservableList<Grade> listOfGrades);

}
