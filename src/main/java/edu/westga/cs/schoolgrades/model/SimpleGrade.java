package edu.westga.cs.schoolgrades.model;

/**
 * This is the SimpleGrade class. It will give a grade a numeric value
 * @author Joey Marinelli
 * @version 10/24/18
 *
 */
public class SimpleGrade implements Grade {
	private double numericGrade;
	
	/**
	 * This is the constructor of the SimpleGrade class. It will accept a numeric value for the grade. 
	 * @param numericGrade
	 */
	public SimpleGrade(double numericGrade) {
		if (numericGrade < 0) {
			throw new IllegalArgumentException("Grade must be a positive number");
		}
		this.numericGrade = numericGrade;
	}
	
	/**
	 * This is the setGrade method, it will change the value of a SimpleGrade object
	 * @param numericGrade
	 */
	public void setGrade(double numericGrade) {
		if (numericGrade < 0) {
			throw new IllegalArgumentException("Grade must be a positive number");
		}
		this.numericGrade = numericGrade;
	}
	
	/**
	 * This method will return the numeric value of the grade. 
	 * @return the numeric grade value
	 */
	@Override
	public double getValue() {
		// TODO Auto-generated method stub
		return this.numericGrade;
	}

}
