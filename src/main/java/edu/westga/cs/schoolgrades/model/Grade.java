package edu.westga.cs.schoolgrades.model;

/**
 * This is the grade interface that will be implemented by the SimpleGrade, CompositeGrade and WeightedGrade classes
 * @author Joey Marinelli
 * @version 10/24/18
 *
 */
public interface Grade {
	public double getValue();
}
