package edu.westga.cs.schoolgrades.model;

import javafx.collections.ObservableList;

/**
 * This is the SumOfGradesStrategy class. It will apply the strategy to take the sum of all the grades in the list. 
 * @author Joey Marinelli
 * @version 10/24/18
 *
 */
public class SumOfGradesStrategy implements Strategy {
	
	/**
	 * THis method will return the composite score which is the sum of the grades in the list. 
	 */
	@Override
	public double getComposite(ObservableList<Grade> listOfGrades) {
		double total = 0.0;
		for(Grade current : listOfGrades) {
			total += current.getValue();
		}
		
		return total;
	}

}
