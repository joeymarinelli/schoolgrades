package edu.westga.cs.schoolgrades.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class DropLowestStrategy implements Strategy {

	private Strategy gradeCalculationStrategy;
	
	public DropLowestStrategy(Strategy gradeCalculationStrategy) {
		if (gradeCalculationStrategy == null) {
			throw new IllegalArgumentException("strategy cannot be null");
		}
		this.gradeCalculationStrategy = gradeCalculationStrategy;
	}
	@Override
	public double getComposite(ObservableList<Grade> listOfGrades) {
		// TODO Auto-generated method stub
		if (listOfGrades == null) {
			throw new IllegalArgumentException("list of grades cannot be null");
		}
		ObservableList<Grade> listOfGradesAfterLowestRemoved = this.dropLowest(listOfGrades);
		return this.gradeCalculationStrategy.getComposite(listOfGradesAfterLowestRemoved);
	}
	
	public ObservableList<Grade> dropLowest(ObservableList<Grade> listOfGrades) {
		
		ObservableList<Grade> copy = FXCollections.observableArrayList(listOfGrades);
		
		Grade lowestGrade = copy.get(0);
		
		if (copy.size() <= 1) {
			return copy;
		} else {
			for (Grade current : copy) {
				if (current.getValue() <= lowestGrade.getValue()) {
					lowestGrade = current;
				}
			}

			copy.remove(lowestGrade);
		
			return copy;
		}
	}
}
