package edu.westga.cs.schoolgrades.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * This is the CompositeGrade class. It will get a composite grade based off of one strategy
 * @author Joey Marinelli
 * @version 10/24/18
 *
 */
public class CompositeGrade implements Grade {
	
	private Strategy gradingStrategy;
	private ObservableList<Grade> listOfGrades;
	
	/**
	 * This is the constructor of the composite grade class. 
	 * @param gradingStrategy is the strategy that composite grade will use to get a grade value
	 */
	public CompositeGrade(Strategy gradingStrategy) {
		if (gradingStrategy == null) {
			throw new IllegalArgumentException("CompositeGrade must have a grading strategy");
		}
		
		this.gradingStrategy = gradingStrategy;
		this.listOfGrades = FXCollections.observableArrayList();
	}
	
	/**
	 * This method will return the list of grades from the strategy 
	 * @return listOfGrades
	 */
	public ObservableList<Grade> getListOfGrades() {
		return this.listOfGrades;
	}
	
	/**
	 * This method will set the grading strategy
	 */
	public void setStrategy(Strategy gradingStrategy) {
		if (gradingStrategy == null) {
			throw new IllegalArgumentException("strategy cannot be null");
		}
		this.gradingStrategy = gradingStrategy;
	}
	/**
	 * This method will add a grade to a list of grades
	 * @param individualGrade is the grade to be added
	 */
	public void addGrade(Grade individualGrade) {
		this.listOfGrades.add(individualGrade);
	}
	
	/**
	 * This method will return the compositeGrade from the selected strategy
	 * @return composite grade value
	 */
	@Override
	public double getValue() {
		return this.gradingStrategy.getComposite(this.listOfGrades);
	}

}
